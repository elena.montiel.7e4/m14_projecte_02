import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'favorites',
        loadChildren: () => import('../pages/favorites/favorites.module').then(m => m.FavoritesPageModule)
      },
      {
        path: 'info',
        loadChildren: () => import('../pages/info/info.module').then(m => m.InfoPageModule)
      },
      {
        path: 'films',
        loadChildren: () => import('../pages/films/films.module').then(m => m.FilmsPageModule)
      },
      {
        path: 'film-details',
        loadChildren: () => import('../pages/film-details/film-details.module').then(m => m.FilmDetailsPageModule)
      },
      {
        path: 'characters',
        loadChildren: () => import('../pages/characters/characters.module').then(m => m.CharactersPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/films',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/splash',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
