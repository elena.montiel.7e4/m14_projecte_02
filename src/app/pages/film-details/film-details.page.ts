import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilmsService } from 'src/app/services/films.service';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.page.html',
  styleUrls: ['./film-details.page.scss'],
})
export class FilmDetailsPage implements OnInit {
  film_details: any;
  id: any;

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private filmsService:FilmsService ) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    //alert(id);
    this.http.get<FilmsService>('https://ghibliapi.herokuapp.com/films/'+id).subscribe(resultados => {
     this.film_details = resultados;
     console.log(this.film_details);
     });
     
  }
}

