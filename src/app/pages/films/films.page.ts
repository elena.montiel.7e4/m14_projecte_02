import { Component, OnInit } from '@angular/core';
import { FilmsService } from 'src/app/services/films.service';
import { HttpClient } from '@angular/common/http';

import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-films',
  templateUrl: './films.page.html',
  styleUrls: ['./films.page.scss'],
})
export class FilmsPage implements OnInit {
  film: any;
  id: any;

  constructor(private http: HttpClient, private filmsService:FilmsService, private navCtrl: NavController, private router: Router) { 
    this.filmsService;
  }

  ionChange(event) {
      console.log(event.detail.value);
  }

  filmDetails(id){
    this.router.navigate(['/film-details/' + id]);
  } 

  ngOnInit() {
     this.http.get<FilmsService>('https://ghibliapi.herokuapp.com/films').subscribe(resultados => {
      this.film = resultados;
      console.log(this.film);
      })
  }
}

