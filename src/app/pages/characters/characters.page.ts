import { Component, OnInit } from '@angular/core';
import { CharactersService } from 'src/app/services/characters.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.page.html',
  styleUrls: ['./characters.page.scss'],
})
export class CharactersPage implements OnInit {

  characters: any;
  cards: any;

  constructor(private http: HttpClient, private charactersService: CharactersService ) { }

  ionChange(event) {
      console.log(event.detail.value);
  }

  ngOnInit() {
    this.http.get<CharactersService>('https://ghibliapi.herokuapp.com/people').subscribe(resultados => {
      this.characters = resultados;
      console.log(this.characters);
      })
  }
}
