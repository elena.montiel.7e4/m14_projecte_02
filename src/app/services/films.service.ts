import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Films} from '../interfaces/films'

@Injectable({
  providedIn: 'root'
})

export class FilmsService {

  constructor(private http: HttpClient) { }

  getFilms() {
    return this.http.get<Films[]>(`https://ghibliapi.herokuapp.com/films`)
  }
  getDetails(id) {
    return this.http.get(`https://ghibliapi.herokuapp.com/films/${id}`);
  }
}
