import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CharactersService {

  constructor(private http: HttpClient) { }

  getCharacters() {
    return this.http.get(`https://ghibliapi.herokuapp.com/people`)
  }
}
